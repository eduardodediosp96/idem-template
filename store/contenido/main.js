export const state = () => ({
  heroMainSlides: [{
      text: "IDEM es una organización sin fines de lucro que tiene como misión descentralizar lacalidad educativa.",
      imageUrl: "/Home/Fotos-IDEM--01.png"
    },
    {
      text: "Logramos nuestro objetivo mediante el desarrollo de cursos presenciales en provincia y cursos online.",
      imageUrl: "/Home/Fotos-IDEM--02.png"
    },
    {
      text: "Nuestros cursos se encuentran especializados en negocios y áreas afines como contabilidad, derecho, economía, entre otros.",
      imageUrl: "/Home/Fotos-IDEM--03.png"
    },
  ],
  Beneficts: [{
      id: 0,
      text: "Aprenderas a tu propio ritmo",
      imageUrl: "/beneficios/1.png"
    },
    {
      id: 1,
      text: "Los mejores profecionales te enseñarán",
      imageUrl: "/beneficios/2.png"
    },
    {
      id: 2,
      text: "Obtendrás un certificado oficial.",
      imageUrl: "/beneficios/3.png"
    },
    {
      id: 3,
      text: "Formarás parte de una comunidad que genera el cambio.",
      imageUrl: "/beneficios/4.png"
    }
  ],
  Testimonials: [{
      text: "Pude culminar mis estudios sin tener que viajar”",
      author: "Jorge Castro"
    },
    {
      text: "Me alegró saber que al capacitarme, personas con menores recursos obtendrían beneficios",
      author: "Christian Paredes"
    },
  ],
  blogNews: [{
      name: 'page_1',
      content: [{
          title: "EL COMERCIO",
          text: "Me alegró saber que al capacitarme, personas con menores recursos obtendrían beneficios",
        },
        {
          title: "EL COMERCIO",
          text: "Me alegró saber que al capacitarme, personas con menores recursos obtendrían beneficios",
        },
        {
          title: "EL COMERCIO",
          text: "Me alegró saber que al capacitarme, personas con menores recursos obtendrían beneficios",
        },
      ]
    },
    {
      name: 'page_2',
      content: [{
          title: "EL COMERCIO",
          text: "Me alegró saber que al capacitarme, personas con menores recursos obtendrían beneficios",
        },
        {
          title: "EL COMERCIO",
          text: "Me alegró saber que al capacitarme, personas con menores recursos obtendrían beneficios",
        },
        {
          title: "EL COMERCIO2",
          text: "Me alegró saber que al capacitarme, personas con menores recursos obtendrían beneficios",
        },
      ]
    }
  ],
})
export const getters = {
  getHeroMainSlides(state) {
    return state.heroMainSlides
  },
  getBenefitsInformation(state) {
    return state.Beneficts
  },
  getTestimonials(state) {
    return state.Testimonials
  },
  getblogNews(state) {
    return state.blogNews
  },
}
